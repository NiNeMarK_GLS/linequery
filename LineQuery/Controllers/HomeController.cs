﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace LineQuery.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public string GetProfile(string UserID)
        {
            //string TokenString = "Bearer daaxZ+EfKDpwSal3w6DjxRl9hBnFPj8aaQAATxfgo7wdKw1fadDtF1x+WyoesaAnWrh8VoD0VUqWreuQLA+LuA9smzcjH+l+CDv9rpPFzpkctSgChZDdTh5HUZP+LTzkqCo85Cc90Qq0b7Fm1Ztg4QdB04t89/1O/w1cDnyilFU=";
            ////https://api.line.me/v2/bot/profile/{userId}
            string TokenString = ConfigurationManager.AppSettings["TokenString"];
            string Url = ConfigurationManager.AppSettings["Line_getProfile"];

            string WEBSERVICE_URL = Url + UserID;
            

            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("Authorization",TokenString);
            client.BaseAddress = new Uri(WEBSERVICE_URL);

            HttpResponseMessage response = client.GetAsync(WEBSERVICE_URL).Result;
            string res = "";
            using (HttpContent content = response.Content)
            {
                // ... Read the string.
                var result = content.ReadAsStringAsync();
                res = result.Result;
            }

           
            return res;
        }
    }
}